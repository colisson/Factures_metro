#!/usr/bin/env python3

### For usage questions, please read README.org

### TODO
# - Générer fichier tex ?
# - Mieux gérer les arrondis

# Regexp
import re
import sys

default_tva = 'B'

tva_convert = {'B': 1.055, 'D': 1.20, 'N': 1}

# Structure, ex :
# [Categorie]
# # Commentaire
# # prix
# 8.4
# # D pour le type de la TVA (default = B)
# 8.4 D
# # prix TVA Quantité Conditionnement
# 8.4 D 30 2
def parse_file(file_name):
    """Parse a given file name into a dict of (a list of dict)"""
    f = open(file_name, 'r')
    dic = {}
    current_cat = ""
    dic_conf = {"Numero": "???", "Date":"???"
                , "Total_ht":"0", "Total_tva":"0"}
    for line in f:
        line = line.strip()
        # Check the categorie
        cat_s = re.match(r"^\[(.*)\]", line)
        if cat_s is not None:
            current_cat = cat_s.group(1)
        elif current_cat == "Conf":
            l = re.split(r"=", line)
            if len(l) == 2:
                dic_conf[l[0].strip()] = l[1].strip()
        elif len(line) > 0 and line[0] != '#':
            l = re.split(r" +", line)
            if len(l) >= 1:
                price = float(l[0])
                tva = tva_convert[default_tva if len(l) < 2 else l[1]]
                qte = 1 if len(l) < 3 else int(l[2])
                conditionnement = 1 if len(l) < 4 else int(l[3])
                if not (current_cat in dic):
                    dic[current_cat] = []
                dic[current_cat] += [{'Price': price
                                     ,'TVA':  tva
                                     ,'QTE':  qte
                                     ,'COND': conditionnement}]
    return (dic,dic_conf)

def treat_dic(dic,dic_conf):
    """Display the informations of the bill"""
    print("########## Facture ##########")
    for k in dic_conf:
        print("%s : %s" % (k, dic_conf[k]))
    print("")
    total = 0
    total_ht = 0
    total_no_round = 0
    total_ht_no_round = 0
    for cat in dic:
        print("==== %s ====" % cat)
        price = 0
        price_ht = 0
        for el in dic[cat]:
            price += el['Price'] * el['TVA'] * el['QTE'] * el['COND']
            price_ht += el['Price'] * el['QTE'] * el['COND']
        total += round(price,2)
        total_ht += round(price_ht,2)
        total_no_round += price
        total_ht_no_round += price_ht
        print("HT : %.2f" % price_ht)
        print("TTC : %.2f\n" % price)
    print("==> Total HT  : %.2f     (Théorique : %s, non arrondie : %s)" % (total_ht, dic_conf["Total_ht"], total_ht_no_round))
    print("(différence de %.2f TTC)" % (float(dic_conf["Total_ht"]) - total_ht))
    print("==> Total TTC : %.2f     (Théorique : %s, non arrondie : %s)" % (total, dic_conf["Total_tva"], total_no_round))
    print("(différence de %.2f TTC)" % (float(dic_conf["Total_tva"]) - total))

    
if __name__ == '__main__':
    if len(sys.argv) >= 2:
        (dic,dic_conf) = parse_file(sys.argv[1])
        # treat_dic_conf(dic_conf)
        treat_dic(dic,dic_conf)
    else:
        print("Usage: ./facture_bde.py <fichier>")
